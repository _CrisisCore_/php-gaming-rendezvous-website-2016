<?php include '../view/header.php'; ?>
<main>
    <nav>
        <h1><?php $team = $teams[0]; echo $team['team_name']; ?> </h1>

        <h2>Members</h2> 
        <ul>
            <?php foreach ($teams as $team) : ?> 
                <li> <?php echo $team['first_name'] . ' ' . $team['last_name'] ?> </li>  
            <?php endforeach; ?>
        </ul> 
    </nav>
</main>
<?php include '../view/footer.php'; ?>