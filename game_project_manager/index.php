<?php

require('../model/database.php');
require('../model/game_db.php');

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'list_games';
    }
}

if ($action == 'list_games') {
    // Get product data
    //$games = get_games();
    $distinct_games = get_genres();
    //used for the table loop
    $filtered_games = get_games();

    // Display the game list
    include('game_list.php');

    //Display the game add page
} else if ($action == 'show_add_form') {
    include('game_add.php');

    //Add the fields of text to the database
} else if ($action == 'add_game') {
    $game_name_to_add = filter_input(INPUT_POST, 'game_name_to_add');
    $genre_to_add = filter_input(INPUT_POST, 'genre_to_add');
    $team_id_to_add = filter_input(INPUT_POST, 'team_id_to_add');
    $project_id_to_add = filter_input(INPUT_POST, 'project_id_to_add');
    // Validate the inputs
    if ($game_name_to_add === NULL || $genre_to_add === NULL ||
            $project_id_to_add === NULL || $team_id_to_add === NULL ) {
        $error = "Invalid product data. Check all fields and try again.";
        include('../errors/error.php');
    } else {
        add_game($project_id_to_add, $game_name_to_add, $genre_to_add, $team_id_to_add);
        header("Location: .");
    }
} else if ($action == 'filter_by_genre') {
    $distinct_games = get_genres();
    $genre = filter_input(INPUT_POST, 'genre');
    if ($genre == "all") {
        $filtered_games = get_games();
    } else {
        $filtered_games = get_games_filter_by_genre($genre);
    }

    include('game_list.php');

    //Display the correct team
} else if ($action == 'list_teams') {
    $teamID = filter_input(INPUT_POST, 'teamID');
    if ($teamID == NULL) {
        include('all_teams.php');
    } else {
        $teams = get_members_by_ID($teamID);
        include('team_list.php');
    }
}
?>