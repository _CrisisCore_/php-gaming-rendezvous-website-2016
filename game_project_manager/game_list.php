<?php include '../view/header.php'; ?>
<main>
    <h1> Search by genre </h1>
    <form action="." method="post">
        <input type="hidden" name="action" value="filter_by_genre">
        <!-- The action prompts to insert the values of genre into this select box,
        and there are 3 possible genres to filter until late website updates -->
        <select name="genre" id="genre">
            <?php foreach ($distinct_games as $game) : ?>
                <!-- Here I get the genre and if it equals the current selected genre, the select box will display the current genre accordingly. -->
                <option <?php if (filter_input(INPUT_POST, 'genre') == $game['genre']) {
                echo ("selected");
            } ?> value ="<?php echo $game['genre'] ?>"><?php echo $game['genre'] ?></option> 
<?php endforeach; ?>
            <option <?php if ((filter_input(INPUT_POST, 'genre')) == NULL || filter_input(INPUT_POST, 'genre') == 'all') {
    echo ("selected");
} ?> value = "all">all genres</option>
        </select>
        <input type="submit" value="Go">
    </form>
    <h2>Project List</h2>

    <!--display a table of games -->
    <table>
        <tr>
            <th>&nbsp;</th>
            <th>Game Name</th>
            <th>Genre</th>
            <th>Team Name</th>
        </tr>

<?php foreach ($filtered_games as $game) : ?>
            <tr>
                <!-- populates the table with each record and its corresponding data: game name, genre, and team name using
                -- a natural join to the teams table, also dynamically generates the image location for the corresponding images--> 
                <td><img src="/GamingRendezvous/images/<?php echo htmlspecialchars($game['game_image_filename']); ?>" alt="<?php echo htmlspecialchars($game['game_image_filename']); ?>" align="center" height="190" width="145"</td>
                <td><?php echo htmlspecialchars($game['game_name']); ?></td>
                <td><?php echo htmlspecialchars($game['genre']); ?></td>
                <td>
                    <form action="." method="post">
                        <input type="hidden" value="<?php echo htmlspecialchars($game['team_id']); ?>" name="teamID">
                        <input type="hidden" name="action" value="list_teams">
                        <input type="submit" value='<?php echo htmlspecialchars($game['team_name']); ?>'>
                    </form>
                </td>
            </tr>
<?php endforeach; ?>
    </table>
    <p><a href="?action=show_add_form">Add Game</a></p>

</main>
<?php include '../view/footer.php'; ?>