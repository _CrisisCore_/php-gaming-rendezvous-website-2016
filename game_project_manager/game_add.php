<?php include '../view/header.php'; ?>
<main>
    <h1>Add Game</h1>
    <form action="." method="post" id="aligned">
        <input type="hidden" name="action" value="add_game">

        <!-- Add secure text entry in the future --> 
        <!-- Enter values to add a game to the database -->
        <label>Project ID:</label>
        <input type="text" name="project_id_to_add"><br>
        
        <label>Game Name:</label>
        <input type="text" name="game_name_to_add"><br>

        <label>Genre:</label>
        <input type="text" name="genre_to_add"><br>

        <label>team ID:</label>
        <input type="text" name="team_id_to_add"><br>

        <label>&nbsp;</label>
        <input type="submit" value="Add Game" /><br>
    </form>
    <p><a href="?action=list_games">View Game List</a></p>

</main>
<?php include '../view/footer.php'; ?>