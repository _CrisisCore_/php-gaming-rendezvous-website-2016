<?php include 'view/header.php'; ?>
<main>
    <nav>

        <h2>Games</h2>
        <ul>
            <li><a href="game_project_manager">Search by Genre</a></li>
        </ul>

        <h2>User Page</h2>    
        <ul>
            <li><a href="game_project_manager/user_list.php">Your Account</a></li>
        </ul>

        <h2>Teams</h2>
        <ul>
            <li><a href="game_project_manager/index.php?action=list_teams">View Teams</a></li>
        </ul>

    </nav>
</section>
<?php include 'view/footer.php'; ?>