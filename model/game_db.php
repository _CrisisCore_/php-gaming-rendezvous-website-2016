<?php
// Add a brand new game
function add_game($project_id_to_add, $game_name_to_add, $genre_to_add, $team_id_to_add) {
    global $db;
    $query = 'INSERT INTO game_projects
                 (project_id, game_name, genre, team_id)
              VALUES
                 (:project_id_to_add, :game_name_to_add, :genre_to_add, :team_id_to_add)';
    $statement = $db->prepare($query);
    $statement->bindValue(':game_name_to_add', $game_name_to_add);
    $statement->bindValue(':genre_to_add', $genre_to_add);
    $statement->bindValue(':project_id_to_add', $project_id_to_add);
    $statement->bindValue(':team_id_to_add', $team_id_to_add);
    $statement->execute();
    $statement->closeCursor();
}
// Get games for display unfiltered
function get_games() {
    global $db;
    $query = 'SELECT * FROM game_projects
              NATURAL JOIN teams
              LEFT OUTER JOIN images 
              ON game_projects.project_id = images.project_id';
    $statement = $db->prepare($query);
    $statement->execute();
    $games = $statement->fetchAll();
    $statement->closeCursor();
    return $games;
}
//Display games that are filtered
function get_games_filter_by_genre($genre) {
    global $db;
    $query = 'SELECT * FROM game_projects
              NATURAL JOIN teams
              LEFT OUTER JOIN images 
              ON game_projects.project_id = images.project_id
              WHERE genre = :genre';
    $statement = $db->prepare($query);
    $statement->bindValue(':genre', $genre);
    $statement->execute();
    $games = $statement->fetchAll();
    $statement->closeCursor();
    return $games;
}
// Get the genres
function get_genres() {
    global $db;
    $query = 'SELECT DISTINCT genre FROM game_projects
              ORDER BY genre ASC';
    $statement = $db->prepare($query);
    $statement->execute();
    $games = $statement->fetchAll();
    $statement->closeCursor();
    return $games;
}
// Get the users 
function get_users() {
    global $db;
    $query = 'SELECT * FROM users
              ORDER BY first_name';
    $statement = $db->prepare($query);
    $statement->execute();
    $users = $statement->fetchAll();
    $statement->closeCursor();
    return $users;
}
// Get users that belong to a certain team 
function get_members_by_ID($team_id) {
    global $db;
    $query = 'SELECT * FROM teams
              NATURAL JOIN users
              WHERE team_id = :teamID';
    $statement = $db->prepare($query);
    $statement->bindValue(':teamID', $team_id);
    $statement->execute();
    $member_team = $statement->fetchAll();
    $statement->closeCursor();
    return $member_team;
}
?>