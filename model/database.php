<?php
    $dsn = 'mysql:host=localhost;dbname=mydb';
    $username = 'root';
    //$username = 'gr_user';
    $password = '';
    //$password = 'uCXfneunHSHMdPhK'; // or change to whatever your root password is

    try {
        $db = new PDO($dsn, $username, $password);
    } catch (PDOException $e) {
        $error_message = $e->getMessage();
        include('../errors/database_error.php');
        exit();
    }
?>